# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Jože Detečnik <joze.detecnik@3tav.si>, 2020
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-28 13:01+0000\n"
"PO-Revision-Date: 2020-12-25 11:54+0000\n"
"Last-Translator: Jože Detečnik <joze.detecnik@3tav.si>\n"
"Language-Team: Slovenian (http://www.transifex.com/rosarior/mayan-edms/language/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: apps.py:40
msgid "Mailer"
msgstr "Mailer"

#: classes.py:80
msgid "Null backend"
msgstr "Ničelna zapora"

#: events.py:5 permissions.py:5 queues.py:6 settings.py:10
msgid "Mailing"
msgstr "Pošiljanje"

#: events.py:8
msgid "Email sent"
msgstr "Email poslan"

#: forms.py:60 forms.py:120
msgid ""
"Email address of the recipient. Can be multiple addresses separated by comma"
" or semicolon."
msgstr "E-poštni naslov prejemnika. Lahko je več naslovov, ločenih z vejico ali podpičjem."

#: forms.py:62 forms.py:122
msgid "Email address"
msgstr "Email naslov"

#: forms.py:64 workflow_actions.py:36
msgid "Subject"
msgstr "Predmet"

#: forms.py:66 workflow_actions.py:45
msgid "Body"
msgstr "Telo"

#: forms.py:70
msgid "The email profile that will be used to send this email."
msgstr "E-poštni profil, ki bo uporabljen za pošiljanje tega e-poštnega sporočila."

#: forms.py:71 models.py:55 views.py:212 workflow_actions.py:18
msgid "Mailing profile"
msgstr "Poštni profil"

#: forms.py:77
msgid "The driver to use when sending emails."
msgstr "Gonilnik za uporabo pri pošiljanju e-pošte."

#: forms.py:78 models.py:69
msgid "Backend"
msgstr "Backend"

#: links.py:16 links.py:26
msgid "Email document"
msgstr "E-poštni dokument"

#: links.py:22 links.py:30
msgid "Email link"
msgstr "E-poštna povezava"

#: links.py:35
msgid "Create mailing profile"
msgstr "Ustvari poštni profil"

#: links.py:41
msgid "Delete"
msgstr "Izbriši"

#: links.py:47
msgid "Edit"
msgstr "Uredi"

#: links.py:52
msgid "Mailing profiles list"
msgstr "Seznam poštnih profilov"

#: links.py:57 models.py:56
msgid "Mailing profiles"
msgstr "Poštni profili"

#: links.py:63 views.py:236
msgid "Test"
msgstr "Preizkus"

#: literals.py:5
#, python-format
msgid ""
"Attached to this email is the document: {{ document }}\n"
"\n"
" --------\n"
" This email has been sent from %(project_title)s (%(project_website)s)"
msgstr "Temu e-poštnemu sporočilu je priložen dokument: {{dokument}} -------- To e-poštno sporočilo je bilo poslano iz %(project_title)s (%(project_website)s)"

#: literals.py:9
msgid "Document: {{ document }}"
msgstr "Dokument: {{dokument}}"

#: literals.py:11
#, python-format
msgid ""
"To access this document click on the following link: {{ link }}\n"
"\n"
"--------\n"
" This email has been sent from %(project_title)s (%(project_website)s)"
msgstr "Za dostop do tega dokumenta kliknite naslednjo povezavo: {{link}} -------- To e-poštno sporočilo je bilo poslano iz %(project_title)s (%(project_website)s)"

#: literals.py:15
msgid "Link for document: {{ document }}"
msgstr "Povezava do dokumenta: {{dokument}}"

#: mailers.py:19 mailers.py:108
msgid "From"
msgstr "Od"

#: mailers.py:22 mailers.py:111
msgid ""
"The sender's address. Some system will refuse to send messages if this value"
" is not set."
msgstr "Naslov pošiljatelja. Nekateri sistem zavrne pošiljanje sporočil, če ta vrednost ni nastavljena."

#: mailers.py:28
msgid "Host"
msgstr "Strežnik"

#: mailers.py:30
msgid "The host to use for sending email."
msgstr "Gostitelj za pošiljanje e-pošte."

#: mailers.py:35
msgid "Port"
msgstr "Vrata"

#: mailers.py:37
msgid "Port to use for the SMTP server."
msgstr "Vrata za strežnik SMTP."

#: mailers.py:40
msgid "Use TLS"
msgstr "Uporabite TLS"

#: mailers.py:43
msgid ""
"Whether to use a TLS (secure) connection when talking to the SMTP server. "
"This is used for explicit TLS connections, generally on port 587."
msgstr "Ali uporabiti povezavo TLS (varno) med pogovorom s strežnikom SMTP. To se uporablja za eksplicitne povezave TLS, običajno na vratih 587."

#: mailers.py:48
msgid "Use SSL"
msgstr "Uporabite SSL"

#: mailers.py:51
msgid ""
"Whether to use an implicit TLS (secure) connection when talking to the SMTP "
"server. In most email documentation this type of TLS connection is referred "
"to as SSL. It is generally used on port 465. If you are experiencing "
"problems, see the explicit TLS setting \"Use TLS\". Note that \"Use TLS\" "
"and \"Use SSL\" are mutually exclusive, so only set one of those settings to"
" True."
msgstr "Ali uporabiti implicitno povezavo TLS (varno) med pogovorom s strežnikom SMTP. V večini e-poštne dokumentacije se ta vrsta povezave TLS imenuje SSL. Običajno se uporablja na vratih 465. Če imate težave, glejte izrecno nastavitev TLS &quot;Uporaba TLS&quot;. Upoštevajte, da se &quot;Uporabi TLS&quot; in &quot;Uporabi SSL&quot; medsebojno izključujeta, zato nastavite samo eno od teh nastavitev na True."

#: mailers.py:60
msgid "Username"
msgstr "Uporabniško ime"

#: mailers.py:63
msgid ""
"Username to use for the SMTP server. If empty, authentication won't "
"attempted."
msgstr "Uporabniško ime za strežnik SMTP. Če je prazno, preverjanje pristnosti ne bo izvedeno."

#: mailers.py:69
msgid "Password"
msgstr "Geslo"

#: mailers.py:72
msgid ""
"Password to use for the SMTP server. This setting is used in conjunction "
"with the username when authenticating to the SMTP server. If either of these"
" settings is empty, authentication won't be attempted."
msgstr "Geslo za strežnik SMTP. Ta nastavitev se uporablja skupaj z uporabniškim imenom pri preverjanju pristnosti na strežniku SMTP. Če je katera od teh nastavitev prazna, preverjanje pristnosti ne bo izvedeno."

#: mailers.py:81
msgid "Django SMTP backend"
msgstr "Django SMTP zaledje"

#: mailers.py:103
msgid "File path"
msgstr "Pot do datoteke"

#: mailers.py:118
msgid "Django file based backend"
msgstr "Django, ki temelji na datoteki"

#: models.py:32
msgid "A short text describing the mailing profile."
msgstr "Kratko besedilo, ki opisuje poštni profil."

#: models.py:33
msgid "Label"
msgstr "Oznaka"

#: models.py:37
msgid ""
"If default, this mailing profile will be pre-selected on the document "
"mailing form."
msgstr "Če je privzeto, bo ta poštni profil vnaprej izbran na poštnem obrazcu za dokument."

#: models.py:39
msgid "Default"
msgstr "Privzeto"

#: models.py:41
msgid "Enabled"
msgstr "Omogočeno"

#: models.py:44
msgid "The dotted Python path to the backend class."
msgstr "Pikčasta pot Pythona do zalednega razreda."

#: models.py:45
msgid "Backend path"
msgstr "Backend pot"

#: models.py:48
msgid "Backend data"
msgstr "Backend podatki"

#: models.py:70
msgid "The backend class for this entry."
msgstr "Razred za ta vnos."

#: models.py:209
msgid "Test email from Mayan EDMS"
msgstr "Preizkusite e-pošto Mayan EDMS"

#: permissions.py:8
msgid "Send document link via email"
msgstr "Pošlji povezavo do dokumenta po e-pošti"

#: permissions.py:11
msgid "Send document via email"
msgstr "Pošljite dokument po e-pošti"

#: permissions.py:14
msgid "Create a mailing profile"
msgstr "Ustvarite poštni profil"

#: permissions.py:17
msgid "Delete a mailing profile"
msgstr "Izbrišite poštni profil"

#: permissions.py:20
msgid "Edit a mailing profile"
msgstr "Uredite poštni profil"

#: permissions.py:23
msgid "View a mailing profile"
msgstr "Oglejte si poštni profil"

#: permissions.py:26
msgid "Use a mailing profile"
msgstr "Uporabite poštni profil"

#: queues.py:8
msgid "Send document"
msgstr "Pošlji dokument"

#: settings.py:15
msgid "Template for the document email form subject line."
msgstr "Predloga za vrstico z zadevo e-poštnega obrazca dokumenta."

#: settings.py:21
msgid "Template for the document email form body text. Can include HTML."
msgstr "Predloga za osnovno besedilo e-poštnega obrazca dokumenta. Lahko vključuje HTML."

#: settings.py:27
msgid "Template for the document link email form subject line."
msgstr "Predloga za vrstico z naslovom e-poštnega obrazca za povezavo do dokumenta."

#: settings.py:33
msgid "Template for the document link email form body text. Can include HTML."
msgstr "Predloga za osnovno besedilo e-poštnega obrazca povezave do dokumenta. Lahko vključuje HTML."

#: validators.py:12
#, python-format
msgid "%(email)s is not a valid email address."
msgstr "%(email)s ni veljaven e-poštni naslov."

#: views.py:39
#, python-format
msgid "%(count)d document queued for email delivery"
msgstr "%(count)d dokument v čakalni vrsti za dostavo po e-pošti"

#: views.py:41
#, python-format
msgid "%(count)d documents queued for email delivery"
msgstr "%(count)d dokumenti v čakalni vrsti za dostavo po e-pošti"

#: views.py:52
msgid "Send"
msgstr "Pošlji"

#: views.py:98
#, python-format
msgid "%(count)d document link queued for email delivery"
msgstr "%(count)d Povezava do dokumenta je v čakalni vrsti za dostavo e-pošte"

#: views.py:100
#, python-format
msgid "%(count)d document links queued for email delivery"
msgstr "%(count)d Povezave do dokumentov so v čakalni vrsti za dostavo e-pošte"

#: views.py:109
msgid "New mailing profile backend selection"
msgstr "Nova izbira zaledja poštnega profila"

#: views.py:141
#, python-format
msgid "Create a \"%s\" mailing profile"
msgstr "Ustvarite poštni profil &quot;%s&quot;"

#: views.py:168
#, python-format
msgid "Delete mailing profile: %s"
msgstr "Izbriši poštni profil: %s"

#: views.py:180
#, python-format
msgid "Edit mailing profile: %s"
msgstr "Uredi poštni profil: %s"

#: views.py:207
msgid ""
"Mailing profiles are email configurations. Mailing profiles allow sending "
"documents as attachments or as links via email."
msgstr "Poštni profili so e-poštne konfiguracije. Poštni profili omogočajo pošiljanje dokumentov kot priponk ali kot povezav po e-pošti."

#: views.py:211
msgid "No mailing profiles available"
msgstr "Na voljo ni nobenega poštnega profila"

#: views.py:228
msgid "Test email sent."
msgstr "Poskusno e-poštno sporočilo je poslano."

#: views.py:237
#, python-format
msgid "Test mailing profile: %s"
msgstr "Preskusni poštni profil: %s"

#: workflow_actions.py:20
msgid "Mailing profile to use when sending the email."
msgstr "Poštni profil za uporabo pri pošiljanju e-pošte."

#: workflow_actions.py:25
msgid "Recipient"
msgstr "Prejemnik"

#: workflow_actions.py:28
msgid ""
"Email address of the recipient. Can be multiple addresses separated by comma"
" or semicolon. A template can be used to reference properties of the "
"document."
msgstr "E-poštni naslov prejemnika. Lahko je več naslovov, ločenih z vejico ali podpičjem. Predloga se lahko uporablja za sklicevanje na lastnosti dokumenta."

#: workflow_actions.py:39
msgid "Subject of the email. Can be a string or a template."
msgstr "Predmet e-pošte. Lahko je niz ali predloga."

#: workflow_actions.py:48
msgid "Body of the email to send. Can be a string or a template."
msgstr "Telo e-poštnega sporočila, ki ga želite poslati. Lahko je niz ali predloga."

#: workflow_actions.py:55
msgid "Send email"
msgstr "Pošlji sporočilo"
